const mongoose  = require('mongoose');
const config = require('./config');
const url = config.db.databaseUrl

const connectWithDB = () => {
  mongoose.Promise = global.Promise;
  mongoose.connect(url)
}

module.exports = connectWithDB;
