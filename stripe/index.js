const config = require('../config')
const stripe = require("stripe")(config.stripe.secret_key);


const createChargeFor = async function (cents, token, email, description) {
  const charge = await stripe.charges.create({
    amount: cents,
    currency: "usd",
    source: token, // obtained with Stripe.js
    receipt_email: email,
    description: description,
    statement_descriptor: "Modern Philosophy.org",
    metadata: {
      "email": email
    }
  }) /* {
    idempotency_key: "uM8mkM4yyz5uokOm"
  })*/

  return charge
}


module.exports = createChargeFor
