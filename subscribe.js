const {json, text} = require('micro')
const config = require('./config')

const User = require('./models/User');
const sendMail = require('./mailgun')


module.exports = async function (req, res) {

  try {
    const data = await json(req)
    if (! data.email) { throw new InvalidJSONError('No Email field in JSON') }

    //Create New User
    const newUser = new User(data);
    const user = await newUser.save()

    //Send Welcome Email (to self and new user) TODO:
    const emailMessage = await sendMail('New Form Submission: Modern Philosophy', data.email, 'willgu29@gmail.com')
    console.log(emailMessage)
    const confirmEmail = await sendMail('Please confirm your email address', 'Link to confirm', )

    return user
  }
  catch (err) {
    console.log(err)
    return err
  }

}


function InvalidJSONError(message) {
  const err = new Error();
  this.message = message
  this.name = 'InvalidJSONError'
  this.code = 400
  this.stack = err.stack
}
