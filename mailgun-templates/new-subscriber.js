
const subscribe = function (secret) {
  const generatedLink = `https://www.modernphilosophy.org/auth/confirm-email?code=${secret}`
  const plain = `Please click the following link in order to confirm your subscription to ModernPhilosophy.org: ${generatedLink}`
  const html = `<p>Please click the following link in order to confirm your subscription to ModernPhilosophy.org: <a href="${generatedLink}">${generatedLink}</a></p>`
  const data = {
    subject: 'Please confirm your email address',
    text: plain,
    html: html
  }
  return data
}

module.exports = subscribe
