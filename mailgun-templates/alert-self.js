
const alert = function (email) {
  const plain = `${email}`
  const html = plain
  const data = {
    subject: 'New Subscription: Modern Philosophy',
    text: plain,
    html: html
  }
  return data
}

module.exports = alert
