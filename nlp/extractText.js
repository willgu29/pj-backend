const textract = require('textract');


const extractText = async function (url) {
  return getTextFromUrl(url)
}

const getTextFromUrl = function (url) {
  return new Promise(resolve => {
    textract.fromUrl(url, function (error, text) {
      if (error) { throw new Error(error)}
      resolve(text)
    });
  });
}

module.exports = extractText
