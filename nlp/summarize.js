const summary = require('../lib/summary');
const sumBasic = require('../lib/sumbasic');
const nlp = require('compromise')

const textBlob = require('../models/textBlob');
const getSentiment = require('./sentiment');

const summarize = function (text) {

  const length = text.length / 5 //average characters per word + 0.5
  console.log(length)

  const doc = nlp(text)
  const sentences = doc.sentences().data()
  const nouns = doc.nouns().out('frequency').slice(0,10)
  const verbs = doc.verbs().out('frequency').slice(0,10)
  const adjectives = doc.adjectives().out('frequency').slice(0,10)

  const top_nouns = nouns.filter(object => object.count > 1)
  const top_verbs = verbs.filter(object => object.count > 1)
  const top_adjectives = adjectives.filter(object => object.count > 1)

  const result = sumBasic([text], length*0.1)
  console.log(result)

  const sentiment = getSentiment(text)

  return {
    summary: result,
    nouns: top_nouns,
    verbs: top_verbs,
    adjectives: top_adjectives,
    sentences: sentences,
    sentiment: sentiment
  }
}

module.exports = summarize

// summary.summarize('This is an example title', text, function(err, summary) {

//   console.log(summary);
//   console.log("Original Length " + text.length);
//    console.log("Summary Length " + summary.length);
//    console.log("Summary Ratio: " + (100 - (100 * (summary.length / text.length))));
// });
//
// summary.getSortedSentences(text, 5, function(err, sorted_sentences) {
//     if(err) {
//         console.log("There was an error."); // Need better error reporting
//     }
//
//     console.log(sorted_sentences);
//   });
