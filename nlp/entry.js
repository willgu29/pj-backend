const nlp = require('compromise')


const getWords = function (text) {

  const doc = nlp(text)
  const nouns = doc.nouns().out('frequency')
  const verbs = doc.verbs().out('frequency')
  const adjectives = doc.adjectives().out('frequency')

  return {
    nouns: nouns,
    verbs: verbs,
    adjectives: adjectives
  }

}

module.exports = getWords
