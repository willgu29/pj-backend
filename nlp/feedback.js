const {json, text, send} = require('micro')
const retext = require('retext');
const nlp = require('compromise');

// const simplifyModule = require('retext-simplify');
const readabilityModule = require('retext-readability');
const passiveModule = require('retext-passive');

const simplifyModule = require('../lib/simplify');
const intensifyModule = require('../lib/intensify');
const usageModule = require('../lib/usage');
const clichesModule = require('../lib/cliches');

const report = require('vfile-reporter');

const textBlob = require('../models/textBlob');

const simplify  = async function (text, cb) {
  retext()
  .use(simplifyModule)
  .process(text, function (err, file) {
    console.log(String(file));
    console.error(report(err || file));
    cb(file || err)
  });
}

const intensify = async function (text, cb) {
  retext()
  .use(intensifyModule)
  .process(text, function (err, file) {
    cb(file || err)
  });
}

const readability = async function (text, age, difficulty, cb) {
  if (! age) { age = 16 } //default
  if (! difficulty) { difficulty = 1/7 }
  retext()
  .use(readabilityModule, {age: age, threshold: difficulty, minWords: 5})
  .process(text, function (err, file) {
    cb(file || err)
  });
}


const usage = async function (text, cb) {
  retext()
    .use(usageModule)
    .process(text, function (err, file) {
      cb(file || err)
    })
}

const cliches = async function (text, cb) {
  retext()
    .use(clichesModule)
    .process(text, function (err, file) {
      cb(file || err)
    })
}

const passive = async function (text, cb) {
  retext()
    .use(passiveModule)
    .process(text, function (err, file) {
      cb(file || err)
    })
}

const deepAnalyzeSentence  = async function (text, cb) {
  retext()
    .use(simplifyModule)
    .use(intensifyModule)
    .use(usageModule)
    .use(clichesModule)
    .use(passiveModule)
    .process(text, function (err, file ) {
      cb(file || err)
    })
}

module.exports = {
  simplify,
  intensify,
  readability,
  usage,
  cliches,
  passive,
  deepAnalyzeSentence
}
