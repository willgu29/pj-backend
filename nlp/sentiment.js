const Sentiment = require('sentiment');


const getSentiment = function (text) {

  var sentiment = new Sentiment();
  var result = sentiment.analyze(text);
  return result

}

module.exports = getSentiment
