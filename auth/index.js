const jwt = require('jsonwebtoken');
const config = require('../config')

const getTokenWithEmail = function (email) {

  const token = jwt.sign({ email: email }, config.options.secret, {
    expiresIn: 86400 // expires in 24 hours
  });

  return token
}

module.exports = {
  getTokenWithEmail
}
