const {json, text, send} = require('micro')
const { router, get, post, put, options, withNamespace } = require('microrouter')
const config = require('./config');

//*** Non-Core Libaries *********************
const mongoose = require('mongoose')

const getSubscribeTemplate = require('./mailgun-templates/new-subscriber')
const getAlertTemplate = require('./mailgun-templates/alert-self')


//*** Imports from Local Files **************
const sendMail = require('./mailgun')
const setHeaders = require('./setHeaders')
const subscribe = require('./subscribe')
const createChargeFor = require('./stripe/index')

//NLP
const entry = require('./nlp/entry')
const extractText = require('./nlp/extractText')
const feedback = require('./nlp/feedback')
const getSentiment = require('./nlp/sentiment')
const summarize = require('./nlp/summarize')

//Auth
const auth = require('./auth/index')
const checkToken = require('./checkToken')

const bcrypt = require('bcrypt');

//*******************************************

//*** Connect to DB *******

const Article = require('./models/Article');
const Quote = require('./models/Quote');
const User = require('./models/User');
const Feedback = require('./models/Feedback');
const Vote = require('./models/Vote');
const Card = require('./models/Card');
const Work = require('./models/Work');


const connectWithDB = require('./db');
connectWithDB();

//*** Routes *******

const api = withNamespace('/api')

module.exports = router(
  api(options('*', (req, res) => checkCors(req, res))),
  api(get('/', (req, res) => send(res, 200, 'Hello, welcome to API.'))),

  api(post('/charge', async (req, res) => {
    try {
      const data = await json(req)
      const charge = await createChargeFor(data.cents, data.token, data.email, data.description)
      send(setHeaders(res), 200, { success: true, charge: charge })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/subscribe', async (req, res) => {
    try {
      const data = await json(req)
      if (! data.email) { throw new InvalidJSONError('Please enter a valid JSON email.') }

      //Create New User
      const newUser = new User(data); //email, subscriptions
      const user = await newUser.save()

      //Send confirmation email
      const subscribeTemplate = getSubscribeTemplate(user.secret)
      const confirmEmail = await sendMail(subscribeTemplate.subject, subscribeTemplate.text, subscribeTemplate.html, data.email)
      console.log(confirmEmail)
      send(setHeaders(res), 200, { success: true, user: user })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }

  })),

  api(post('/unsubscribe', async (req, res) => {
    try {
      const data = await json(req)
      if (! data.email) { throw new InvalidJSONError('Please enter a valid JSON email.') }
      //unsubscribe user email
      const user = await User.findOneAndUpdate({ email: data.email }, { subscriptions: [] }, { new : true })
      send(setHeaders(res), 200, { success: true, user: user })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(put('/user/:id', async (req, res) => {
    const data = await json(req)
    const id = req.params.id
    const updatedUser = await User.findByIdAndUpdate(id, { $set: data }, { new: true })
    send(setHeaders(res), 200, { success: true, user: updatedUser })
  })),

  api(post('/feedback', async (req, res) => {
    const data = await json(req)
    if (! data.message) { throw new ErrorMessage(`Please include a message. ${data}`, 'Invalid JSON') }
    const newFeedback = new Feedback(data) //from, path, message, private
    const feedback = await newFeedback.save()
    send(setHeaders(res), 200, { success: true, feedback: feedback })
  })),

  api(post('/vote', async (req, res) => {
    const data = await json(req)
    if (! data.score) { throw new ErrorMessage(`Please include a score. ${data}`, 'Invalid JSON') }
    if (! data.path) { throw new ErrorMessage(`Please include a path. ${data}`, 'Invalid JSON') }
    const newVote = new Vote(data)
    const vote = await newVote.save()
    send(setHeaders(res), 200, { success: true, vote: vote })
  })),

  api(post('/tokens', async (req, res) => {
    try {
      const data = await json(req)
      if (! data._id) { throw new ErrorMessage(`Please include a user. ${data}`, 'Invalid JSON') }
      if (! data.amount) { throw new ErrorMessage(`Please include an amount. ${data}`, 'Invalid JSON') }
      if (! data.path) { throw new ErrorMessage(`Please include a path for token. ${data}`, 'Invalid JSON') }
      const newTransaction = {
        amount: data.amount,
        path: data.path,
        description: data.description
      }
      const user = await User.findOneAndUpdate({ _id: data._id }, { $push: { tokens: newTransaction }}, { new: true })
      send(setHeaders(res), 200, { success: true, tokens: user.tokens })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/articles', async (req, res) => {
    try {
      const data = await json(req)
      const newArticle = new Article(data)
      const article = await newArticle.save()
      send(setHeaders(res), 200, { success: true, article: article })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/quotes', async (req, res) => {
    try {
      const data = await json(req)
      const newQuote = new Quote(data)
      const quote = await newQuote.save()
      send(setHeaders(res), 200, { success: true, quote: quote })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/cards', async (req, res) => {
    try {
      const data = await json(req)
      const newCard = new Card(data)
      const card = await newCard.save()
      send(setHeaders(res), 200, { success: true, card: card })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/emails', async (req, res) => {
    try {
      const data = await json(req)
      if (data.password !== 'modern123') { throw new ErrorMessage('Wrong Password', "Invalid Password") }
      const result = await sendMail(data.subject, data.text, data.html, data.to, data.users)
      console.log(result)
      send(setHeaders(res), 200, { success: true })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(post('/nlp/extractText', async (req, res) => {
    const data = await json(req)
    const result = await extractText(data.url)
    send(setHeaders(res), 200, result)
  })),
  api(post('/nlp/feedback/simplify', async (req, res) => {
    const data = await json(req)
    feedback.simplify(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/intensify', async (req, res) => {
    const data = await json(req)
    feedback.intensify(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/readability', async (req, res) => {
    const data = await json(req)
    feedback.readability(data.text, data.age, data.difficulty, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/usage', async (req, res) => {
    const data = await json(req)
    feedback.usage(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/cliches', async (req, res) => {
    const data = await json(req)
    feedback.cliches(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/passive', async (req, res) => {
    const data = await json(req)
    feedback.passive(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/feedback/sentence', async (req, res) => {
    const data = await json(req)
    feedback.deepAnalyzeSentence(data.text, result => send(setHeaders(res), 200, result))
  })),
  api(post('/nlp/sentiment', async (req, res) => {
    const data = await json(req)
    const sentiment = getSentiment(data.text)
    send(setHeaders(res), 200, sentiment)
  })),
  api(post('/nlp/summarize', async (req, res) => {
    const check = checkToken(req)
    if (! check) {
      send(setHeaders(res), 401, 'Not authorized')
      return
    }
    const data = await json(req)
    const result = summarize(data.text)
    send(setHeaders(res), 200, result)
  })),
  api(post('/nlp/wordBreakdown', async (req, res) => {
    const data = await json(req)
    console.log('start words!')
    const words = entry(data.text)
    console.log('done words!')
    send(setHeaders(res), 200, words)
  })),


  api(get('/articles', async (req, res) => {
    try {
      const articles = await Article.find({}, 'title description slug createdAt')
      send(setHeaders(res), 200, { success: true, articles: articles })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/articles/:group(/:title)', async (req, res) => {
    try {
      const slug = '/' + ['articles', req.params.group, req.params.title].filter(str => str).join('/')
      const article = await Article.findOne({ slug: slug })
      send(setHeaders(res), 200, { success: true, article: article })
    } catch (err) {
      send(setHeaders(res), 404, { success: false, message: 'Article not found.' })
    }
  })),

  api(get('/cards', async (req, res) => {
    try {
      const cards = await Card.find()
      send(setHeaders(res), 200, { success: true, cards: cards })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),


  api(get('/cards/:id', async (req, res) => {
    try {
      const id = req.params.id
      const card = await Card.findById(id)
      send(setHeaders(res), 200, { success: true, card: card })
    } catch (err) {
      send(setHeaders(res), 404, { success: false, message: 'Card not found.' })
    }
  })),

  api(get('/works', async (req, res) => {
    try {
      const works = await Work.find()
      send(setHeaders(res), 200, { success: true, works: works })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/quotes', async (req, res) => {
    try {
      console.log(req.query.option)
      const isAll = req.query.option === 'all'
      const fields = isAll ? undefined : 'title group description slug work.content'
      const quotes = await Quote.find({}, fields).populate('work.content')
      send(setHeaders(res), 200, { success: true, quotes: quotes })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/quotes/random', async (req, res) => {
    try {
      const quoteCount = await Quote.count()
      const random = Math.floor(Math.random() * quoteCount)
      const quote = await Quote.findOne().skip(random).populate('work.content')
      send(setHeaders(res), 200, { success: true, quote: quote })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/quotes/:author/:title', async (req, res) => {
    try {
      const author = req.params.author
      const slug = '/' + ['wiki', decodeURIComponent(author), req.params.title].join('/')
      const quote = await Quote.findOne({ slug: slug }).populate('work.content')
      send(setHeaders(res), 200, { success: true, quote: quote })
    } catch (err) {
      send(setHeaders(res), 404, { success: false, message: 'Quote not found.' })
    }
  })),

  api(get('/users', async (req, res) => {
    try {
      const users = await User.find({}, 'email emailVerified firstName lastName subscriptions scopes')
      send(setHeaders(res), 200, { success: true, users: users })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/users/:id', async (req, res) => {
    try {
      const user = await User.findById(req.params.id, 'email emailVerified firstName lastName scopes subscriptions bio')
      send(setHeaders(res), 200, { success: true, user: user })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),

  api(get('/auth/confirm-email', async (req, res) => {
    try {
      const code = req.query.code
      if (! code) { throw new ErrorMessage(`expected query, code`, 'query not found') }
      const saved = await User.findOneAndUpdate({secret: code}, {emailVerified: true}, { new: true })
      send(setHeaders(res), 200, { success: true, user: saved })
    } catch (err) {
      send(setHeaders(res), 400, { success: false, message: err.message })
    }
  })),
  api(get('/auth/user', async (req, res) => {

    try {
      const check = checkToken(req)
      console.log(check)
      if (! check.email) {
        send(setHeaders(res), 200, { success: false, user: null })
        return
      }
      const user = await User.findOne({email: check.email}).select('-password -secret')
      send(setHeaders(res), 200, { success: true, user: user })
    } catch (err) {
      send(setHeaders(res), 404, { success: false, message: err.message } )
    }
  })),
  api(post('/auth/login', async (req, res) => {
    try {
      const data = await json(req)
      const user = await User.findOne({email: data.email}).select('+password')
      if (! user) { throw new ErrorMessage(`No user found by ${data.email} email`, "NotFound Error") }
      const result = await user.validPassword(data.password)
      if (! result) { throw new ErrorMessage("Incorrect password", 'BcryptHash') }
      const token = auth.getTokenWithEmail(data.email)
      send(setHeaders(res), 200, { success: true, token: token })
    } catch (err) {
      send(setHeaders(res), 401, { success: false, message: err.message })
    }
  })),
  api(post('/auth/create', async (req, res) => {
    try {
      const { email, password, firstName, lastName } = await json(req)
      const found = await User.findOne({email: email})
      if (found) {
        const hash = await hashPassword(password)
        const saved = await User.findOneAndUpdate({email: email}, {password: hash, firstName, lastName}, { new: true })
        send(setHeaders(res), 200, saved)
      } else {
        const newUser = new User({ email, password , firstName, lastName })
        const result = await newUser.save()
        const subscribeTemplate = getSubscribeTemplate(result.secret)
        const confirmEmail = await sendMail(subscribeTemplate.subject, subscribeTemplate.text, subscribeTemplate.html, result.email)
        send(setHeaders(res), 200, result)
      }
    } catch (err) {
      send(setHeaders(res), 400, err)
    }
  })),
  api(post('/auth/logout', async (req, res) => {
    send(setHeaders(res), 200, {success: true})
  })),


  //must be at end
  api(get('*', (req, res) => send(res, 404, 'Not Found')))

)

//*******************

const checkCors = (req, res) => {
  if (config.options.nodeEnv === 'development') {
    send(setHeaders(res), 200, 'Passed')
  }
    ((req.headers.origin === 'https://www.modernphilosophy.org' ||
      req.headers.origin === 'https://www.penguinjeffrey.com') &&
      req.headers['access-control-request-method'] === 'POST')
        ? send(setHeaders(res), 200, 'Passed') : send(res, 401, 'Unauthorized')

}

function ErrorMessage(message, name) {
  const err = new Error();
  this.message = message
  this.name = name
  this.stack = err.stack
}

const SALT_ROUNDS = 8;

function hashPassword(password) {
  return new Promise(resolve => {
    bcrypt.genSalt(SALT_ROUNDS, function(err, salt) {
        if (err) throw new ErrorMessage(err, 'BcryptGenSalt Failed')

        // hash the password using our new salt
        bcrypt.hash(password, salt, function(err, hash) {
            if (err) throw new ErrorMessage(err, 'BcryptHash Failed')
            // override the cleartext password with the hashed one
            resolve(hash)
        });
    });
  });
}
