const {json, text, send} = require('micro')
const config = require('./config')


var mailgun = require('mailgun-js')({apiKey: config.mailgun.private_key, domain: config.mailgun.domain});

module.exports = async function (subject, body, html, to, users) {
  const from = config.mailgun.from

  try {
    if (users && users.length > 1) {
      console.log('batch')
      const result = await sendBatch(users, subject, body, html)
      return result
    } else {
      console.log('single')
      const result = await sendMail(subject, body, html, from, to)
      return result
    }
  }

  catch (err) {
    console.log(err)
    return err
  }

}

const sendMail = async (subject, body, html, from, to) => {
  var data = {
    from: from,
    to: to,
    subject: subject,
    text: body,
    html: html
  };

  return (config.options.nodeEnv === 'development')
  ? { success: true, message: "Email sent! (But not really since dev)", data: data }
  : await mailgun.messages().send(data)
}

const sendBatch = async (users, subject, body, html) => {
  const to = users.map(user => user.email)
  const object = {}
  const variables = users.map(user => {
    object[user.email] = { _id: user._id, firstName: user.firstName || 'there'}
    return ''
  })
  const data = {
    from: config.mailgun.from,
    to: to.join(', '),
    subject: subject,
    text: body,
    html: html,
    'recipient-variables': object
  }
  console.log(data['recipient-variables'])
  return (config.options.nodeEnv === 'development')
  ? { success: true, message: "Email sent! (But not really since dev)", data: data }
  : await mailgun.messages().send(data)

}
