const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const FeedbackSchema = new Schema({
  from: {
    type: String,
    default: '',
    trim: true
  },
  path: {
    type: String
  },
  message: {
    type: String,
    required: true,
    trim: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  private: {
    type: Boolean,
    default: true
  }
});


const initColl = () => {
  if(mongoose.models.Feedback) {
    return mongoose.model('Feedback')
  }
  else {
    return mongoose.model('Feedback', FeedbackSchema);
  }
}
module.exports = initColl();
