const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const VoteSchema = new Schema({
  score: {
    type: Number,
    required: true
  },
  path: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});


const initColl = () => {
  if(mongoose.models.Vote) {
    return mongoose.model('Vote')
  }
  else {
    return mongoose.model('Vote', VoteSchema);
  }
}
module.exports = initColl();
