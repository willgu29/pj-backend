const mongoose  = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_ROUNDS = 8;

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

var Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
    index: {
      unique: true,
      dropDups: true
    },
    required: 'Please specify an email',
    trim: true
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  password: {
    type: String,
    select: false
  },
  firstName: {
    type: String,
    default: '',
    trim: true
  },
  lastName: {
    type: String,
    default: '',
    trim: true
  },
  joined: {
    type: Date,
    default: Date.now
  },
  referred: {
    emails: { type: [String], default: [] },
    code: { type: String, default: guid }
  },
  secret: {
    type: String,
    default: guid,
    select: false
  },
  token: { //for api
    type: String,
    select: false
  },
  stripeId: {
    type: String
  },
  subscriptions: {
    type: [String], //articles, projects, quotes
    default: []
  },
  scopes: {
    type: [String],
    default: ['general'] //'general', 'privileged', 'magistrate', 'infinite'
  },
  tokens: {
    type: [{
      amount: Number, //can be neg
      path: {
        type: String, //no trailing
        validate: {
          validator: function(v) {
            return v.slice(-1) !== '/'
          },
          message: 'Remove trailing / from {VALUE} !'
        },
        required: 'Please include a token url path.'
      },
      description: String,
      created: {
        type: Date,
        default: Date.now
      }
    }], //append transcations, concat for current amount
    default: []
  },
  bio: {
    type: String
  }
});

UserSchema.path('email').validate({
  isAsync: true,
  validator: function(value, done) {
    this.model('User').count({ email: value }, function(err, count) {
        if (err) {
            return done(err);
        }
        // If `count` is greater than zero, "invalidate"
        done(!count);
    });
  },
  message:'Email already exists'
});

// Hash passwords on first time save
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_ROUNDS, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

// checking if password is valid
UserSchema.methods.validPassword = async function(password) {
  const match = await bcrypt.compare(password, this.password);
  return match
};

const initColl = () => {
  if(mongoose.models.User) {
    return mongoose.model('User')
  }
  else {
    return mongoose.model('User', UserSchema);
  }
}
module.exports = initColl();
