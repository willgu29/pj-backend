const textBlob = `It was her who first introduced me to this damned concept, this circle, but I found her words so beautiful and her equally too.

She was one of those fabled good girls who lived with faith and knew just the right thing to do. Remarkable, always remarkable as it stands in contrast to all those other girls who become lost in the filth of the world, vengeful so I think... and this one had stepped so far out of the bounds of the vengefulness, or at least, that is all I could perceive with my deft eyes.

I find myself contemplating all the time who I am and who I've met, I find that it changes all too frequently and yet, is never altogether different. How can that be the case? We talk of change all the time, how this has changed, I have changed, we have changed, the future! A place with change, with things that exist that would be better left for everyone... well? Do things really change?

I'm trying to encapsulate some ideas onto a piece of virtual paper, and it always stops me with how poorly they find themselves, there's never a focus, so it never amounts to anything, but sometimes I think that if I just kept typing somehow it would coalscece for me and the strain of actually having to think about this work would fly past me. I'd arrive at the pinnacle of art, art in motion, and that idea excites me.

For you see, the artist generally takes to having to find himself in the "zone" in order to make art. He or she sets up routine, habit, and by doing so finds the creative muse visit at those times allotted and set aside for precisely that work. It is in this way that almost all artists are restrained from the onset: they must find the will to draw and set words on pages first!

Then of course, comes the revision process, for the onset of words and art are never quite right, several attempts need to be taken on precisely the same subject matter, and even then it never seems to reach a pinnacle of beauty like those of the great works. Ha! And those great works, how many of them were just flukes of the imagination, warranted only by a sort of familiarity of verse by the time the author was late of age and by the 100th monkey effect surely found some sort of... "over-bubbling" of ideas that accumulated into something that sticks.

Fine. Perhaps that argument is not to convincing, and yet, what am I supposed to convince myself with? There is no convincing in this world, there are only shapes and forms, such a delusion of reality and unreality that one seldom knows what to trust or believe in.

And that alone would not be a problem, but it finds itself another problem that there is just too much time, such ample amounts of time to be filled that, try as one may, it is just impossible to fill it! What could one possibly do with all this time, and worse yet, the higher one's intelligence the harder it becomes to fill that time for boredom creeps in much too quickly and ennui sets off in flight at the first sign of some reprieve.

And ironically, this is contrastly strongly with the fact that things move incredibly slow. So slow in fact that one would seldom think that a person could live in these inbetween periods of waiting and seeing what the conclusions will be. Hospital lines are long, text messages take forever to exchange and accumulate into simple events, and people, oh people are so slow to weather relations.

I hate to wait, and I'd like to progress at my own pain and pace yet the world presents all these sorts of blockers and materials in the way to... "level the crowd." There is no such concept as skipping grades, though there are kids who skip grades... somehow it is never a formalized process, no one can truly warrant themselves to find a way to skip grades, no, it is all timed, timed to the death and to each regardless of their ability to await the time that will find itself as the last pinnacle of that which needs to be done.

Oh time! But that is why I have arrived back at this damned circle...

For you see, I have become convinced, so entirely convinced, that the pull of this circle is so inexplicably strong that there may be no possible way to escape it except by succumbing to it even stronger.

Let me explain. For we may not like to think of this topic, however, it seems to me that almost all of it, if not all of surely, find ourselves repeating the same mistakes, doing the same things, and finding the same conclusions time and time again, almost never, almost definitely never, altering those results and if we do happen to alter those results only find that it was a progression to a larger circle, a sort of cycling of different results within the same results paradigm of a circle. A larger circle. A larger repetition.

And it is at this point that I myself do not even wish to think of this topic anymore for I find if I do that I may find no reason for existing outside of this idea at all! It is a terrible idea for it makes the man nothing more than... a circle: and that circle I fear in its urgency and unwillingness to become anything other than a circle.

No no, we will have to return to this another time.

---

### The Halls Lined Up:

Welcome dear reader, to a fantastical tale of tales that spanned across the ages of the ages. Before you lies the definite text, the truest edition, and therefore the proper tale about the unfortunate son of a widow and the girl with whom he met one day.

Yes yes, love! Love it comes! That is this tale, but do not be deceived if we are to expect tragedy or miracles, happy endings or shocking truths. No, no: for this is the stuff of fairy tales, whom tell of finality and closure of sorts, but this love, no there will be no form of such.

He from a motley family who worked hard to gain their stature within society, pride and prudence were set aside to be able to work a farm on lands that were all too baren with a burden that rivaled olden times were food was scarce and the only pleasure one found was the pleasure from working the farm.

Two glasses of milk a day from the cow, a few barrels of corner and enough wheat to pass the day. One cigar, alotted to the oldest man in the house, and the rest of the day spent resting for the next work day to come.

This is the routine that each and every family that lived around the farms had gotten used to at this time, for the Industrial Revolution was just beginning, and the times had not caught up to everyone, no, it had not caught up to everyone.

Yet our lad on the farm lands, we'll call him Isaac, was an astute lad, and though he was not well-educated (as he could not be as he had to work the farm), he was a curious boy and would take to reading whatever he got his hands on, to try to discern from the images and other sorts of things what was going on, how the world had worked, and what it all meant to him.^[And I'll throw this away too!]


Society asks us to mask our differences by the form of social politeness. This is cowardice at its finest.

ModernPhilosophy asks for 3 primary values:
1. Simplicity
2. Honesty
3. Consistency

Without which there can be no foundation to build the next 3 secondary values that follow:

1. "Attempting life"
2. "Pursuing one's own path"
3. "Values to Details"

The goal is simple: fuck this world and how it is, it needs to exist as something else.

I hate waste and not "building the start" of the system correctly, but this is just what has to be done. Fix the other problems later. Fix the first problem, which is that I don't really have any good problems to speak of.

"Good problems" - These problems amount from actually doing things, of which I'm doing nothing right now. I exist: Alone. And I could keep doing this for the rest of my life and yet this seems like a failing strategy at best.

I am talented when directed towards solving other people's god damn problems. At worst I'm cynical, at best, I inspire: though I do not believe in inspiration, though I suppose it holds merit for others.


I just need to return to the roots and ask myself at the core what is the simplest statement that ModernPhilosophy can offer? And from that statement I can work upwards.

The first thing that comes to mind is progress. Just progress, ride the bicycle so to say. Life happens in stages and so does philosophical thought, things will always change...

Or honesty...

==Or maybe I can describe it with a narrative.== (for ModernPhilosophy.org)


### A Few Attempts at Narrative:

I won't begin! I cannot begin! The external motivations and factors that weigh on my mind prevent me from undertaking nothing in particular but always beg to undertake more than I think I can undertake! Simplicity calls for some other sake, intensity, rash judgment! [Hark! Goes the ashes!](/rambling-1/).

Alright, here's what we do, here's how we spend our time...

Imagine, you're a little child and you don't even know how you came to be. You just remember one day that you started having memories and things happened and now you're here and that's just about all you know, and yet, things are not to be questioned, things are fine, they really are and there's so much to do, it's not a question, it's just... reality.

And you continually grow up and eventually your narrative begins to form more cohesively, you think you remember more, and maybe you do, and because of this you begin to understand more, or at least you think you do. You begin to remember that you already went to the park yesterday, and maybe it wasn't all that fun, or that two years ago you were spending time with Ali and now you're spending time with Joseph, but you really preferred Ali because she was nicer though Joseph is kind of alright too at least when he sings for you.

Each year more of these observations happen, more and more memories of these recollections (rather than the things themselves) begin to form and hit sort of a bowling over point. You can feel it, the mind is wrestling with something that it really wasn't supposed to wrestle with, one day: "you wake up."

You begin to examine you past life in reflection and you begin to understand something that is at once terrifying and at once brilliant for what it means, this idea, this idea that has probably been thrown at your for a long time but only now began to make sense finally appears: **what is your future going to be like?**

You see, in formation, the brain only processed the present, until it had enough material to sort of form some condensed ideas about the past, but neither of those are super terrifying, they are what is and what has been, and what is will always be what is, and what has been will always be what has been, but what will be... oh my, of the possibilities, what will be cannot be imagined, predicted, though we try.

We become enamored with this "great" beyond, this thing that captures our wasteland hearts and asks for us to go past this moment into the future, and we extend ourselves, are able to see for once and question, "why was I doing that? why did this happen? why... am I here?"

These questions form likely on the basis of this expansion of scope, the man is removed from himself, and what is revealed is a sort of hollow lens of existence. Death comes into question for the first time. Beginnings come into question for the first time. The frame is set, the world **begins.**

From that day forward comes with it a terrible but also freeing gift: the potential to surmount circumstances, to change, to **build a new future,** whether that is locally for the individual or globally for the masses, regardless, its this future, this idea of the future that begins to plague us, we are aware of it, and can never become unaware again, though some of us try.

It is with little experience that these teenagers have to begin to make sense of their lives and themselves, of others and their place in the world, of the unknown, of the certain, of all the plausible ideas that follow from this grand awakening. It is consciousness, it is the first real experience of consciousness that knows itself.

Teens will begin to turn to their parents, to their friends, teachers, movies, music, books, material, knowledge, anything that can help them form the basis of **something.** Without which the world, is hell.

Of course, this is not the case for those teens who had good families, in which "living", the skills of it, are passed on appropriately. Routine, rote, will be fine for them.

But for the rest, the future, oh the future it calls them, and it scares them. For if the future will always be like what it is now... what eternal present such as this could be worth living in?

So the beacon of delusions begin, the call of consciousness starts. To which path do you follow... and this will depend on the perceived goal (which does not at all correlate to the root consequence).

We begin to make certainties of ourselves and of the future where there can be none, but we do this because we cannot not think of this continual future, this sort of devil.^[So we need to reconcile this future blah blah]

"help the children" narrative? "good of the good" narrative?


"The no narrative narrative?" There has to be an external narrative, even if there isn't an internal one... it's only we have of understanding "purpose", of why and what and therefore how.

Look, then keep it stupid simple: people need philosophy, the role of philosophy has always been to guide humanity based on what has stood as eternal, what is simple. If it has become complicated and morose, it is because of a progression in time that has pushed philosophy further and further away from the domains of real existence, of the average person with whom philosophy first started: amongst the people.

We imagine Socrates going from person to person in order to share with them his knowledge (however correct or incorrect) in a spirit of openness. Of connected intellect. The world's intellect. A connected node.

That is ModernPhilosophy, a return to connectedness.

So what do we do? We connect. We contextualize. Ideas, people, life. Life as connected to each and every occurence, the web of knowledge around a particular topic seen as of utmost relevance.

A hierarchy:

Title == main subject
Content of post --> Snippets from it, key phrases/topics as the "related/connected content".

`

module.exports = textBlob;
