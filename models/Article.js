const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const ArticleSchema = new Schema({
  title: {
    type: String,
    required: 'Please enter a title',
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  slug: {
    type: String,
    required: "Please enter a slug url",
    index: {
      unique: true,
      dropDups: true
    },
    trim: true
  },
  markdown: {
    type: String,
    trim: true,
    required: 'Please enter markdown for article.'
  },
  group: {
    type: String,
    trim: true,
    lowercase: true
  }
}, { timestamps: true });


const initColl = () => {
  if(mongoose.models.Article) {
    return mongoose.model('Article')
  }
  else {
    return mongoose.model('Article', ArticleSchema);
  }
}
module.exports = initColl();
