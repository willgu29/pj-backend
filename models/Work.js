const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const WorkSchema = new Schema({
  title: {
    type: String,
    trim: true,
    required: 'Please etner a work title.'
  },
  author: {
    type: String,
    trim: true,
    required: 'Please enter a author for work.'
  },
  genre: String,
  source: String
});



const initColl = () => {
  if(mongoose.models.Work) {
    return mongoose.model('Work')
  }
  else {
    return mongoose.model('Work', WorkSchema);
  }
}
module.exports = initColl();
