const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const CardSchema = new Schema({
  markdown: {
    type: String,
    trim: true,
    required: 'Please enter markdown content.'
  },
  preview: {
    type: String,
    trim: true
  },
  slug: {
    type: String, //if '/', means 'journal snippet'
    required: "Please enter a slug url",
    trim: true
  },
  group: {
    type: String,
    trim: true,
    lowercase: true
  },
  tags: [{
    type: String,
    trim: true,
    lowercase: true
  }],
  importance: {
    type: Number,
    default: 0 //maybe cap at 100
  }

}, { timestamps: true });


const initColl = () => {
  if(mongoose.models.Card) {
    return mongoose.model('Card')
  }
  else {
    return mongoose.model('Card', CardSchema);
  }
}
module.exports = initColl();
