const mongoose  = require('mongoose');
var Schema = mongoose.Schema;

const QuoteSchema = new Schema({
  title: {
    type: String,
    required: 'Please enter a title',
    trim: true
  },
  description: {
    type: String, //max 60 characters
    trim: true
  },
  slug: {
    type: String,
    required: "Please enter a slug url",
    index: {
      unique: true,
      dropDups: true
    },
    trim: true
  },
  markdown: {
    type: String,
    trim: true,
    required: 'Please enter markdown for quote.'
  },
  group: {
    type: String,
    trim: true,
    lowercase: true,
    default: 'miscellaneous'
  },
  work: {
    content: {
      type: Schema.Types.ObjectId,
      ref: 'Work',
      required: 'Please enter a work content _id'
    },
    pageNumber: String
  }
}, {timestamps: true } );


QuoteSchema.path('description').validate(function (str) {
  return str.length < 76
}, 'The max length of description is 75 characters.')

const initColl = () => {
  if(mongoose.models.Quote) {
    return mongoose.model('Quote')
  }
  else {
    return mongoose.model('Quote', QuoteSchema);
  }
}
module.exports = initColl();

/*

explanations: [
  {
    title: {
      type: String,
      trim: true
    },
    markdown: {
      type: String,
      trim: true
    }
  }
]
*/
