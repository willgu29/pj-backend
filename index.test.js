import micro from 'micro';
import test from 'ava';
import request from 'request-promise'

const ROOT_URL = function (endpoint) {
  return `http://localhost:4299/api${endpoint}`
}

test('/subscribe + /auth/confirm-email', async t => {

  const subscribe = {
    method: 'POST',
    uri: ROOT_URL('/subscribe'),
    body: {
        email: 'test-email@gmail.com'
    },
    json: true // Automatically stringifies the body to JSON
  };
  const subscription = await request(subscribe)

  const confirmEmail = {
    method: 'GET',
    uri: ROOT_URL('/auth/confirm-email'),
    qs: {
      code: subscription.user.secret
    },
    json: true // Automatically stringifies the body to JSON
  };

  const confirmation = await request(confirmEmail)

  t.is(confirmation.user.emailVerified, true)

})

test('/create account + update password on same account', async t => {
  const createAccount = {
    method: 'POST',
    uri: ROOT_URL('/auth/create'),
    body: {
      email: 'new-email@gmail.com',
      password: 'hello',
      firstName: 'Will',
      lastName: 'Gu'
    },
    json: true
  }
  const account = await request(createAccount)
  t.is(account.firstName, 'Will')
  t.not(account.password, 'hello') //hashed?

  const updatePassword = {
    method: 'POST',
    uri: ROOT_URL('/auth/create'),
    body: {
      email: 'new-email@gmail.com',
      password: 'new-password',
      firstName: 'William'
    },
    json: true
  }
  const updatedAccount = await request(updatePassword)
  t.not(updatedAccount.password, account.password)
  t.not(updatedAccount.password, 'new-password')
  t.is(updatedAccount.firstName, 'William')
})
