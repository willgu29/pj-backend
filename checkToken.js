const jwt = require('jsonwebtoken')
const config = require('./config')

module.exports = function (req) {

  const header = req.headers.authorization
  if (header) {
    const split = header.split(' ')
    const token = split[1]

    if (!token) return { auth: false, message: 'No token provided.' }

    try {
      const decoded = jwt.verify(token, config.options.secret);
      return decoded

      //re-new token: (todo)
      // https://gist.github.com/ziluvatar/a3feb505c4c0ec37059054537b38fc48

    } catch (err) {
      return { auth: false, message: 'Failed to authenticate token.' + err }
    }
  }
  return { auth: false, message: "No authorization header found" }

}
