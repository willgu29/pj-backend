const visit = require('unist-util-visit');
const toString = require('nlcst-to-string');
const search = require('nlcst-search');
const quotation = require('quotation');
const difference = require('lodash.difference');
const intersection = require('lodash.intersection');


const cliches = require('./data/cliches');


/**
 * Attacher.
 *
 * @param {Retext} processor
 *   - Instance.
 * @param {Object?} [options]
 *   - Configuration.
 * @param {Array.<string>?} [options.ignore]
 *   - List of phrases to *not* warn about.
 * @return {Function} - `transformer`.
 */

function attacher(processor, options) {
  var ignore = (options || {}).ignore || [];

  return transformer;
    /**
    * Search `tree` for validations.
    *
    * @param {Node} tree - NLCST node.
    * @param {VFile} file - Virtual file.
    */
  function transformer(tree, file) {
    visit(tree, 'ParagraphNode', visitor);

    function visitor(node) {
      search(node, cliches, handle)

      function handle(match, position, parent, phrase) {
        var value = quotation(toString(match), '“', '”');
        var message = 'Warning: ' + value  + ' is a cliche';

        message = file.warn(message, {
          'start': match[0].position.start,
          'end': match[match.length - 1].position.end
        });

        message.cliche = phrase;
        message.source = 'retext-cliche';

      }

    }
  }

}

module.exports = attacher;
