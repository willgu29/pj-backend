const visit = require('unist-util-visit');
const toString = require('nlcst-to-string');
const search = require('nlcst-search');
const quotation = require('quotation');
const difference = require('lodash.difference');
const intersection = require('lodash.intersection');


const patterns = require('./data/usage.json');

let temp = []
let keys = []
const list = Object.keys(patterns).map(function(key){
  const value = patterns[key]
  temp[key] = value
  keys.push(key)
});

/**
 * Attacher.
 *
 * @param {Retext} processor
 *   - Instance.
 * @param {Object?} [options]
 *   - Configuration.
 * @param {Array.<string>?} [options.ignore]
 *   - List of phrases to *not* warn about.
 * @return {Function} - `transformer`.
 */

function attacher(processor, options) {
  var ignore = (options || {}).ignore || [];
  var phrases = difference(temp, ignore);
  var noNormalize = intersection(phrases, []);
  var normalize = difference(phrases, noNormalize);

  return transformer;
    /**
    * Search `tree` for validations.
    *
    * @param {Node} tree - NLCST node.
    * @param {VFile} file - Virtual file.
    */
  function transformer(tree, file) {
    visit(tree, 'ParagraphNode', visitor);

    function visitor(node) {
      console.log(keys)
      search(node, keys, handle)

      function handle(match, position, parent, phrase) {
        var pattern = temp[phrase]
        var replace = pattern.replace;
        if (Array.isArray(replace)) {
          replace = replace.join(', ')
        }
        var value = quotation(toString(match), '“', '”');
        var message;
        if (pattern.omit && !replace.length) {
          message = 'Remove ' + value;
        } else {
          message = 'Replace ' + value + ' with ' + quotation(replace, '“', '”')
          if (pattern.omit) {
            message += ', or remove it';
          }
        }

        message = file.warn(message, {
          'start': match[0].position.start,
          'end': match[match.length - 1].position.end
        });

        message.ruleId = phrase;
        message.source = 'retext-usage';

      }

    }
  }

}




module.exports = attacher;
