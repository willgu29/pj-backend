npm install
npm run dev
npm run start


#### On server:

pm2 start npm --name "pj-backend" -- start
pm2 restart all



#### Other Useful Documentation:

Routing:
https://github.com/pedronauck/micro-router

Text Extraction:
https://github.com/dbashford/textract

Main NLP:
https://github.com/spencermountain/compromise

Text NLP:
https://github.com/retextjs/retext/blob/master/doc/getting-started.md
https://github.com/retextjs/retext/blob/master/doc/plugins.md

Readability Plugin:
https://github.com/retextjs/retext-readability

Summarize NLP: (imported as lib since they failed with npm)
https://github.com/jbrooksuk/node-summary
https://github.com/MSVCode/node-sumbasic

Node Version 8.11.1


---

Additional Thoughts:
https://www.nodebox.net/code/index.php/Linguistics (cool NLP functions)
